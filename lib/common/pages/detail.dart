import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:oddbit_erfin/common/blocs/detail_bloc.dart';
import 'package:oddbit_erfin/common/elements/colors.dart';
import 'package:oddbit_erfin/common/elements/frame.dart';
import 'package:oddbit_erfin/common/elements/request.dart';
import 'package:oddbit_erfin/common/elements/shadow.dart';
import 'package:oddbit_erfin/common/models/movie.dart';
import 'package:provider/provider.dart';

class DetailPage extends StatefulWidget {
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  DetailBloc bloc;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<DetailBloc>(context);
    Movie data = bloc.selectedMovie;
    final DateFormat formatter = DateFormat('dd MMMM yyyy');
    return Frame(
      pageTopPadding: 0,
      icon: StreamBuilder(
          stream: bloc.isFav,
          builder: (context, snapshot) {
            print(snapshot.data);
            if (snapshot.hasData && snapshot.data) {
              return Icon(
                FeatherIcons.heart,
                color: Colors.red,
              );
            }
            return Icon(FeatherIcons.heart);
          }),
      title: Text("${data.title}"),
      body: StreamBuilder<Object>(
          stream: bloc.detail,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              data = snapshot.data;
              List<String> rDate = data.releaseDate.split("-");
              DateTime releaseDate = new DateTime(int.parse(rDate[0]),
                  int.parse(rDate[1]), int.parse(rDate[2]));
              return ListView(
                children: [
                  Stack(
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 260,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        data.backdropPath != null
                                            ? (Request.imageUri +
                                                '/w500' +
                                                data.backdropPath)
                                            : ''))),
                          ),
                          ConstrainedBox(
                            constraints: BoxConstraints(minHeight: 60),
                            child: Container(
                              margin: EdgeInsets.only(left: 120),
                              padding: EdgeInsets.only(top: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    margin: EdgeInsets.only(top: 5),
                                    decoration: BoxDecoration(
                                        boxShadow: OShadow().elevate(),
                                        color: data.voteAverage < 7
                                            ? OdColors.yellow
                                            : OdColors.green,
                                        shape: BoxShape.circle),
                                    child: Text(
                                      "${data.voteAverage}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    child: Wrap(
                                      children: data.genres.map<Widget>((e) {
                                        return Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 1, horizontal: 2),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5, horizontal: 10),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5)),
                                            color: OdColors.darkerBlue,
                                          ),
                                          child: Text(
                                            "${e['name']}",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        top: 170,
                        left: 20,
                        child: Container(
                          height: 150,
                          width: 100,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              image: DecorationImage(
                                  fit: BoxFit.fitHeight,
                                  image: NetworkImage(data.posterPath != null
                                      ? (Request.imageUri +
                                          '/w500' +
                                          data.posterPath)
                                      : ''))),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: OdColors.secondary,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        data.originalTitle != data.title
                            ? Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "${data.title}",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              )
                            : SizedBox(),
                        Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: OdColors.primary,
                          ),
                          child: Text(
                            "${data.originalTitle}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        SizedBox(height: 20),
                        Text(
                          "Released at ${formatter.format(releaseDate)}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "Runtime " + bloc.formatMinutes(data.runtime),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(height: 15),
                        Text(
                          "Overview",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(height: 10),
                        Text(
                          "${data.overview}",
                          style:
                              TextStyle(color: Colors.white.withOpacity(0.9)),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: OdColors.secondary,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Production Companies",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(height: 10),
                        Wrap(
                          children: data.productionCompanies.map<Widget>((e) {
                            return Container(
                              margin: EdgeInsets.all(5),
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                color: OdColors.primary,
                              ),
                              child: Text(
                                "${e['name']}",
                                style: TextStyle(color: Colors.white),
                              ),
                            );
                          }).toList(),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: OdColors.secondary,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Revenue",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(height: 10),
                        Container(
                          margin: EdgeInsets.all(5),
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: OdColors.primary,
                          ),
                          child: Text(
                            "${bloc.formatDollar(data.revenue)}",
                            style: TextStyle(color: Colors.white),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: OdColors.secondary,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Spoken Languages",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(height: 10),
                        Wrap(
                          children: data.spokenLanguages.map<Widget>((e) {
                            return Container(
                              margin: EdgeInsets.all(5),
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                color: OdColors.primary,
                              ),
                              child: Text(
                                "${e['name']}",
                                style: TextStyle(color: Colors.white),
                              ),
                            );
                          }).toList(),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: OdColors.secondary,
                    ),
                    child: Wrap(
                      children: [
                        Container(
                          margin: EdgeInsets.all(5),
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: OdColors.primary,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "Vote Count ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: OdColors.green),
                                child: Text(
                                  "${data.voteCount}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          margin: EdgeInsets.all(5),
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: OdColors.primary,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "Vote Average ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: OdColors.yellow),
                                child: Text(
                                  "${data.voteAverage}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          margin: EdgeInsets.all(5),
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: OdColors.primary,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "Popularity ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    color: OdColors.yellow),
                                child: Text(
                                  "${data.popularity}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }
            return Container();
          }),
      onTap: () {
        bloc.setFavorites(data);
      },
    );
  }
}
