import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:oddbit_erfin/common/blocs/base_bloc.dart';
import 'package:oddbit_erfin/common/blocs/detail_bloc.dart';
import 'package:oddbit_erfin/common/elements/card..dart';
import 'package:oddbit_erfin/common/elements/colors.dart';
import 'package:oddbit_erfin/common/elements/frame.dart';
import 'package:oddbit_erfin/common/models/movie.dart';
import 'package:provider/provider.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class LandingPage extends StatefulWidget {
  LandingPage({Key key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  BaseBloc bloc;
  DetailBloc dbloc;
  showFilterModal(context) {
    showModalBottomSheet(
        builder: (BuildContext context) {
          return Container(
            color: OdColors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: OdColors.darkBlue,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Filters",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      Icon(
                        FeatherIcons.chevronDown,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Release Dates"),
                              SizedBox(
                                height: 5,
                              ),
                              StreamBuilder(
                                  stream: bloc.releaseDates,
                                  builder: (context, snapshot) {
                                    print(snapshot.data);
                                    if (snapshot.hasData) {
                                      final DateFormat formatter =
                                          DateFormat('dd MMMM yyyy');
                                      return Container(
                                        child: Text(
                                          "From ${formatter.format(snapshot.data[0])} to ${formatter.format(snapshot.data[1])}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      );
                                    }
                                    return Text(
                                      "Searching all releases",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    );
                                  })
                            ],
                          ),
                          InkWell(
                              onTap: () async {
                                DateTime today = new DateTime.now();

                                DateTime before =
                                    today.subtract(new Duration(days: 7));

                                final List<DateTime> picked =
                                    await DateRagePicker.showDatePicker(
                                        context: context,
                                        initialFirstDate: before,
                                        initialLastDate: today,
                                        firstDate: new DateTime(2015),
                                        lastDate: today);
                                if (picked != null && picked.length == 2) {
                                  bloc.setReleaseDate(picked);
                                  print(picked);
                                }
                              },
                              child: Icon(FeatherIcons.calendar))
                        ],
                      )),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Sort by"),
                              StreamBuilder(
                                  stream: bloc.sortBy,
                                  builder: (context, snapshot) {
                                    String dropdownValue = 'popularity.desc';
                                    if (snapshot.hasData) {
                                      dropdownValue = snapshot.data;
                                    }

                                    return DropdownButton<String>(
                                      value: dropdownValue,
                                      icon: Icon(Icons.arrow_downward),
                                      iconSize: 24,
                                      elevation: 16,
                                      style:
                                          TextStyle(color: OdColors.darkerBlue),
                                      underline: Container(
                                        height: 2,
                                        color: OdColors.darkerBlue,
                                      ),
                                      onChanged: (String newValue) {
                                        bloc.setSort(newValue);
                                      },
                                      items: <Map>[
                                        {
                                          'popularity.desc':
                                              'Popularity Descending'
                                        },
                                        {
                                          'popularity.asc':
                                              'Popularity Ascending'
                                        },
                                        {
                                          'release_date.desc':
                                              'Release Date Descending'
                                        },
                                        {
                                          'release_date.asc':
                                              'Release Date Ascending'
                                        },
                                        {
                                          'vote_count.desc':
                                              'Vote Count Descending'
                                        },
                                        {
                                          'vote_count.asc':
                                              'Vote Count Ascendings'
                                        },
                                      ].map((value) {
                                        return DropdownMenuItem<String>(
                                          value: value.keys.first,
                                          child: Text(value.values.first),
                                        );
                                      }).toList(),
                                    );
                                  })
                            ],
                          ),
                        ],
                      )),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    bloc.getMovies(context);
                    Navigator.pop(context);
                  },
                  child: Container(
                      margin: EdgeInsets.all(10),
                      color: OdColors.green,
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Search",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      )),
                )
              ],
            ),
          );
        },
        context: context);
  }

  showMyFavorites(contex) {
    bloc.getMyFavourites();
    showModalBottomSheet(
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Container(
              color: OdColors.primary,
              child: Container(
                child: Column(children: [
                  Container(
                      height: 80,
                      padding: EdgeInsets.only(top: 30),
                      width: MediaQuery.of(context).size.width,
                      color: OdColors.secondary,
                      child: Center(
                        child: Text(
                          "Favourites",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              letterSpacing: 1),
                        ),
                      )),
                  Expanded(
                      child: ListView(
                    children: [
                      StreamBuilder(
                          stream: bloc.favMovies,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return GridView.builder(
                                  shrinkWrap: true,
                                  physics: ScrollPhysics(),
                                  itemCount: snapshot.data.length,
                                  gridDelegate:
                                      new SliverGridDelegateWithFixedCrossAxisCount(
                                          mainAxisSpacing: 5,
                                          crossAxisSpacing: 5,
                                          childAspectRatio: 0.5,
                                          crossAxisCount: 2),
                                  itemBuilder: (context, index) {
                                    Movie item = snapshot.data[index];
                                    return InkWell(
                                        onTap: () {
                                          dbloc.setSelected(item);
                                          Navigator.pushNamed(
                                              context, 'detail');
                                        },
                                        child: OCard(data: item));
                                  });
                            }
                            return Container();
                          }),
                    ],
                  ))
                ]),
              ));
        },
        context: context);
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    dbloc = Provider.of<DetailBloc>(context);
    if (bloc.movieList.length < 1) {
      bloc.getMovies(context);
    }

    return Frame(
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("THE MOVIE DB"),
              InkWell(
                onTap: () {
                  showMyFavorites(context);
                },
                child: Icon(
                  FeatherIcons.heart,
                  color: Colors.red,
                ),
              )
            ],
          ),
        ),
        onTap: () {
          showFilterModal(context);
        },
        body: ListView(
          children: [
            StreamBuilder(
                stream: bloc.movies,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return GridView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: snapshot.data.length,
                        gridDelegate:
                            new SliverGridDelegateWithFixedCrossAxisCount(
                                mainAxisSpacing: 5,
                                crossAxisSpacing: 5,
                                childAspectRatio: 0.5,
                                crossAxisCount: 2),
                        itemBuilder: (context, index) {
                          Movie item = snapshot.data[index];
                          return InkWell(
                              onTap: () {
                                dbloc.setSelected(item);
                                Navigator.pushNamed(context, 'detail');
                              },
                              child: OCard(data: item));
                        });
                  }
                  return Container();
                }),
          ],
        ));
  }
}
