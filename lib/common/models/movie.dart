class Movie {
  final dynamic popularity;
  final int voteCount;
  final bool video;
  final String posterPath;
  final int id;
  final bool adult;
  final String backdropPath;
  final String originalLanguage;
  final String originalTitle;
  final List<dynamic> genreIds;
  final List<dynamic> genres;
  final String title;
  final dynamic voteAverage;
  final String overview;
  final String releaseDate;
  final String homepage;
  final int revenue;
  final int budget;
  final int runtime;
  final List<dynamic> spokenLanguages;
  final List<dynamic> productionCompanies;
  final List<dynamic> productionCountries;
  Movie(
      {this.popularity,
      this.voteCount,
      this.posterPath,
      this.id,
      this.adult,
      this.backdropPath,
      this.originalLanguage,
      this.originalTitle,
      this.genreIds,
      this.genres,
      this.title,
      this.voteAverage,
      this.overview,
      this.video,
      this.releaseDate,
      this.revenue,
      this.budget,
      this.spokenLanguages,
      this.productionCompanies,
      this.productionCountries,
      this.homepage,
      this.runtime});

  Map<String, dynamic> toMap() {
    return {
      "popularity": popularity,
      "voteCount": voteCount,
      "posterPath": posterPath,
      "id": id,
      "adult": adult,
      "backdropPath": backdropPath,
      "originalLanguage": originalLanguage,
      "originalTitle": originalTitle,
      "genreIds": genreIds,
      "genres": genres,
      "title": title,
      "voteAverage": voteAverage,
      "overview": overview,
      "video": video,
      "releaseDate": releaseDate,
      "revenue": revenue,
      "budget": budget,
      "spokenLanguages": spokenLanguages,
      "productionCompanies": productionCompanies,
      "productionCountries": productionCountries,
      "homepage": homepage,
      "runtime": runtime
    };
  }

  Movie toClass(Map<String, dynamic> data) {
    return Movie(
        popularity: data["popularity"],
        voteCount: data["voteCount"],
        posterPath: data["posterPath"],
        id: data["id"],
        adult: data["adult"],
        backdropPath: data["backdropPath"],
        originalLanguage: data["originalLanguage"],
        originalTitle: data["originalTitle"],
        genreIds: data["genreIds"],
        genres: data["genres"],
        title: data["title"],
        voteAverage: data["voteAverage"],
        overview: data["overview"],
        video: data["video"],
        releaseDate: data["releaseDate"],
        revenue: data["revenue"],
        budget: data["budget"],
        spokenLanguages: data["spokenLanguages"],
        productionCompanies: data["productionCompanies"],
        productionCountries: data["productionCountries"],
        homepage: data["homepage"],
        runtime: data["runtime"]);
  }
}
