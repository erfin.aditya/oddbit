class Genre {
  final String id;
  final String name;
  Genre(this.id, this.name);
}