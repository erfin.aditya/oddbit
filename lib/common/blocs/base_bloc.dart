import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:oddbit_erfin/common/elements/mySembast.dart';
import 'package:oddbit_erfin/common/elements/request.dart';
import 'package:oddbit_erfin/common/models/movie.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rxdart/subjects.dart';

class BaseBloc extends ChangeNotifier {
  BehaviorSubject _movies = new BehaviorSubject();
  get movies => _movies.asBroadcastStream();

  BehaviorSubject _favMovies = new BehaviorSubject();
  get favMovies => _favMovies.asBroadcastStream();

  List<Movie> _movieList = [];
  get movieList => _movieList;

  List<Movie> _favMovieList = [];
  get favMovieList => _favMovieList;

  BehaviorSubject _releaseDates = new BehaviorSubject();
  get releaseDates => _releaseDates.asBroadcastStream();

  setReleaseDate(List<DateTime> dates) {
    _releaseDates.add(dates);
  }

  BehaviorSubject _sortBy = new BehaviorSubject();
  get sortBy => _sortBy.asBroadcastStream();

  setSort(String sort) {
    _sortBy.add(sort);
  }

  getMyFavourites() async {
    MySembast smb = new MySembast();
    Map<String, dynamic> data = await smb.read("favourites");
    print(data);
    _favMovieList.clear();
    data.forEach((key, value) {
      if (key != null) {
        _favMovieList.add(Movie().toClass(value));
        _favMovies.add(_favMovieList);
      }
    });
  }

  getMovies(context) async {
    Map<String, dynamic> queries = {
      "language": "en-US",
      "sort_by": "popularity.desc",
      "include_adult": true,
      "include_video": false,
      "page": 1
    };

    if (_releaseDates.hasValue) {
      final DateFormat formatter = DateFormat('yyyy-MM-dd');
      queries['release_date.gte'] = formatter.format(_releaseDates.value[0]);
      queries['release_date.lte'] = formatter.format(_releaseDates.value[1]);
    }

    if (_sortBy.hasValue) {
      queries['sort_by'] = _sortBy.value;
    }

    dynamic response = await Request().getData("discover/movie", queries);

    if (response != false) {
      List<dynamic> results = response['results'];
      _movieList.clear();
      for (dynamic item in results) {
        _movieList.add(Movie(
            title: item['title'],
            popularity: item['popularity'],
            voteCount: item['vote_count'],
            posterPath: item['poster_path'],
            id: item['id'],
            adult: item['adult'],
            backdropPath: item['backdrop_path'],
            originalLanguage: item['original_language'],
            originalTitle: item['original_title'],
            genreIds: item['genre_ids'],
            voteAverage: item['vote_average'],
            overview: item['overview'],
            video: item['video'],
            releaseDate: item['release_date']));
      }

      _movies.add(_movieList);
    }
    // pg.hide();
  }
}
