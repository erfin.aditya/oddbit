import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:oddbit_erfin/common/elements/mySembast.dart';
import 'package:oddbit_erfin/common/elements/request.dart';
import 'package:oddbit_erfin/common/models/movie.dart';
import 'package:rxdart/subjects.dart';
import 'package:sembast/utils/value_utils.dart';

class DetailBloc extends ChangeNotifier {
  Movie _selectedMovie;
  get selectedMovie => _selectedMovie;

  MySembast smb = new MySembast();
  PublishSubject _detail = PublishSubject();

  get detail => _detail.asBroadcastStream();

  BehaviorSubject _isFav = BehaviorSubject();
  get isFav => _isFav.asBroadcastStream();

  setSelected(Movie mov) {
    _selectedMovie = mov;
    smb.read("favourites").then((value) {
      if (value != null) {
        Map<String, dynamic> data = value["${mov.id}"];
        print(data);
        if (data != null) {
          _isFav.add(true);
        } else {
          _isFav.add(false);
        }
      } else {
        _isFav.add(false);
      }
    });
    getMovies(_selectedMovie.id);
  }

  getMovies(id) async {
    Map<String, dynamic> queries = {
      "language": "en-US",
    };

    dynamic response = await Request().getData("movie/${id}", queries);
    if (response != false) {
      dynamic item = response;
      _detail.add(Movie(
          title: item['title'],
          popularity: item['popularity'],
          voteCount: item['vote_count'],
          posterPath: item['poster_path'],
          id: item['id'],
          adult: item['adult'],
          backdropPath: item['backdrop_path'],
          originalLanguage: item['original_language'],
          originalTitle: item['original_title'],
          genreIds: item['genre_ids'],
          genres: item['genres'],
          voteAverage: item['vote_average'],
          overview: item['overview'],
          video: item['video'],
          releaseDate: item['release_date'],
          homepage: item['homepage'],
          revenue: item['revenue'],
          budget: item['budget'],
          spokenLanguages: item['spoken_languages'],
          productionCompanies: item['production_companies'],
          productionCountries: item['production_countries'],
          runtime: item['runtime']));
    }
  }

  formatMinutes(int min) {
    int h = min ~/ 60;
    int m = min % 60;
    return (h > 0 ? "${h} Hour(s) " : "") + (m > 0 ? "${m} Minute(s) " : "");
  }

  formatDollar(int number) {
    final cur = new NumberFormat.currency(locale: "en_US");
    return cur.format(number);
  }

  setFavorites(Movie data) async {
    smb.delete("favourites");
    dynamic readFav = await smb.read("favourites");
    print(readFav);

    if (readFav == null) {
      Map<dynamic, dynamic> favData = {"${data.id}": data.toMap()};
      smb.write("favourites", favData);
    } else {
      Map<dynamic, dynamic> existing = cloneMap(readFav);
      if (existing["${data.id}"] == null) {
        existing["${data.id}"] = data.toMap();
        _isFav.add(true);
      } else {
        existing.remove("${data.id}");
        _isFav.add(false);
      }
      smb.write("favourites", existing);
    }
  }
}
