import 'package:oddbit_erfin/common/pages/detail.dart';
import 'package:oddbit_erfin/common/pages/landing.dart';
import 'package:oddbit_erfin/common/pages/splash.dart';
import 'package:provider/provider.dart';

import 'base_bloc.dart';
import 'detail_bloc.dart';

class CustomProvider {
  static var providers = [
    ChangeNotifierProvider<BaseBloc>.value(value: BaseBloc()),
    ChangeNotifierProvider<DetailBloc>.value(value: DetailBloc())
  ];

  static routes(context) {
    return {
      '/': (context) {
        return LandingPage();
      },
      'detail': (context) {
        return DetailPage();
      }
    };
  }
}
