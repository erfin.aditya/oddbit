import 'package:dio/dio.dart';

class Request {
  static var baseUri = "https://api.themoviedb.org/3/";
  String apiKey = "1b869b3ccf57d089047ded4b1de007b8";
  static var imageUri = "http://image.tmdb.org/t/p";

  getData(String portal, Map<String, dynamic> params) async {
    params['api_key'] = apiKey;
    try {
      Response response =
          await Dio().get(baseUri + portal, queryParameters: params);
      return response.data;
    } catch (e) {
      print("ERRORR $e");
      return false;
    }
  }
}
