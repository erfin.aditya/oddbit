import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:oddbit_erfin/common/elements/colors.dart';

class Frame extends StatelessWidget {
  final Widget body;
  final Widget title;
  final Function onTap;
  final Widget icon;
  final double pageTopPadding;
  const Frame(
      {@required this.body,
      @required this.title,
      @required this.onTap,
      this.pageTopPadding,
      this.icon});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: OdColors.secondary,
          title: title,
          elevation: 0,
          actions: [
            InkWell(
              onTap: onTap,
              child: icon ??
                  Icon(
                    FeatherIcons.arrowDownCircle,
                    color: OdColors.white,
                  ),
            ),
            SizedBox(width: 10)
          ],
        ),
        backgroundColor: OdColors.primary,
        body: Padding(
          child: body,
          padding: EdgeInsetsDirectional.only(top: pageTopPadding ?? 5),
        ));
  }
}
