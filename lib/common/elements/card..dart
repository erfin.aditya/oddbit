import 'package:flutter/material.dart';
import 'package:oddbit_erfin/common/elements/colors.dart';
import 'package:oddbit_erfin/common/elements/request.dart';
import 'package:oddbit_erfin/common/elements/shadow.dart';
import 'package:oddbit_erfin/common/models/movie.dart';

class OCard extends StatelessWidget {
  final Movie data;
  const OCard({@required this.data});

  @override
  Widget build(BuildContext context) {
    String shortOverview = data.overview.length > 100
        ? data.overview.substring(0, 100)
        : data.overview;
    return Stack(children: [
      Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: OShadow().elevate(color: OdColors.primary),
            color: OdColors.white,
            image: DecorationImage(
                fit: data.posterPath != null ? BoxFit.cover : BoxFit.contain,
                image: NetworkImage(data.posterPath != null
                    ? (Request.imageUri + '/w185' + data.posterPath)
                    : ''))),
      ),
      Container(
        margin: EdgeInsets.zero,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(top: 5),
                  decoration: BoxDecoration(
                      boxShadow: OShadow().elevate(),
                      color: data.voteAverage < 7
                          ? OdColors.yellow
                          : OdColors.green,
                      shape: BoxShape.circle),
                  child: Text(
                    "${data.voteAverage}",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                data.adult
                    ? Container(
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.only(top: 5),
                        decoration: BoxDecoration(
                            boxShadow: OShadow().elevate(),
                            color: Colors.red,
                            shape: BoxShape.circle),
                        child: Text(
                          "${data.adult ? "17+" : ""}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
            Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: OdColors.darkBlue,
                  borderRadius:
                      BorderRadius.only(topRight: Radius.circular(10))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${data.title}",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 5),
                  Text(
                    "$shortOverview${data.overview.length > 100 ? '....' : ''}",
                    style: TextStyle(
                      fontSize: 12,
                      color: OdColors.white,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    ]);
  }
}
