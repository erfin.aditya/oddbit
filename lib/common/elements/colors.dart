import 'package:flutter/material.dart';

class OdColors {
  static const primary = Color.fromRGBO(26, 26, 26, 1);
  static const secondary = Color.fromRGBO(12, 12, 12, 1);
  static const white = Color.fromRGBO(255, 255, 255, 0.7);
  static const darkBlue = Color.fromRGBO(4, 37, 65, 1);
  static const darkerBlue = Color.fromRGBO(2, 23, 41, 1);
  static const green = Color.fromRGBO(33, 208, 122, 1);
  static const yellow = Color.fromRGBO(211, 213, 48, 1);
  static const grey = Color.fromRGBO(219, 219, 219, 1);
}