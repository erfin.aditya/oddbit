import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

class MySembast {
  String dbPath = 'LocalDB.db';
  DatabaseFactory dbFactory = databaseFactoryIo;
  Database db;

  open() async {
    Directory appDocDirectory = await getApplicationDocumentsDirectory();
    db = await dbFactory.openDatabase(appDocDirectory.path + "/$dbPath");
  }

  write(dynamic variable, dynamic data) async {
    await open();
    var store = StoreRef.main();
    return await store.record(variable).put(db, data);
  }

  add(dynamic variable, dynamic data) async {
    await open();
    var store = StoreRef.main();
    return await store.record(variable).add(db, data);
  }

  Future read(variable) async {
    await open();
    var store = StoreRef.main();
    return await store.record(variable).get(db);
  }

  delete(variable) async {
    await open();
    var store = StoreRef.main();
    await store.record(variable).delete(db);
  }
}
