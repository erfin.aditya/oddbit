import 'package:flutter/material.dart';
import 'package:oddbit_erfin/common/elements/colors.dart';

class OShadow {
  static double dimen = 2;

  List<BoxShadow> elevate({double radius, double offset, Color color}) {
    return [
      BoxShadow(
        spreadRadius: 0,
        color: color != null ? color : OdColors.white,
        offset: Offset(
            offset != null ? offset : dimen, offset != null ? offset : dimen),
        blurRadius: radius != null ? radius : dimen,
      ),
    ];
  }
}
