import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'common/blocs/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      child: MaterialApp(
          initialRoute: '/', routes: CustomProvider.routes(context)),
        
      providers: CustomProvider.providers,
      builder: (context, widget) {
        return widget;
      },
    );
  }
}
